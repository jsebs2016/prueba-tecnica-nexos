package com.nexossoftware.commons;

import java.util.List;

public interface GenericServiceAPI <T,ID>  {
	
	T save(T entity);
	List<T> getAll();
	T get(ID id);
	void delete(ID id);
	
}
