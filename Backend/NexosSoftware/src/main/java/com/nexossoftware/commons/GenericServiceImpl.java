package com.nexossoftware.commons;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public abstract class GenericServiceImpl<T, ID extends Serializable> implements GenericServiceAPI<T, ID> {
	
	@Override
	public T save(T entity) {
		return getDao().save(entity);
	}

	@Override
	public List<T> getAll() {
		List<T> resultList = new ArrayList<>();
		getDao().findAll().stream().forEach(resultList::add);
		return resultList;
	}
	
	@Override
	public T get(ID id) {
		Optional<T> obj = getDao().findById(id);
		if (obj.isPresent()) {
			return obj.get();
		}
		return null;
	}
	
	@Override
	public void delete(ID id) {
		Optional<T> obj = getDao().findById(id);
		getDao().delete(obj.get());
	}
	
	public abstract JpaRepository<T, ID> getDao();

}
