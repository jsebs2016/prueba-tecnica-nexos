package com.nexossoftware.constant;

public enum CodeState {
	CODIGO_EXITOSO("200"),
	CODIGO_ERROR("500");
	
	private final String codigo;

	
	private CodeState(String codigo) {
		this.codigo = codigo;
	}


	public String getCodigo() {
		return codigo;
	}
}
