package com.nexossoftware.constant;

public enum MessageState {
	
	MENSAJE_EXITO_CARGO("Datos de cargo almacenados"),
	MENSAJE_ACTUALIZAR_CARGO("Datos de cargo actualizados"),
	MENSAJE_ERROR_CARGO("No fue posible insertar y/o actualizar cargo"),
	MENSAJE_EXITO_PRODUCTO("Datos de producto almacenados"),
	MENSAJE_ACTUALIZAR_PRODUCTO("Datos de producto actualizados"),
	MENSAJE_ELIMINAR_PRODUCTO("Datos de producto eliminados"),
	MENSAJE_ERROR_PRODUCTO("No fue posible insertar y/o actualizar producto"),
	MENSAJE_EXITO_CONSULTA("Consulta exitosa"),
	MENSAJE_ERROR_CONSULTA("No fue posible consultar"),
	MENSAJE_FALLO_SERVIDOR("Fallo interno de servidor"),
	MENSAJE_EXITO_USUARIO("Datos de usuario almacenados"),
	MENSAJE_ERROR_USUARIO("No fue posible insertar usuario"),
	MENSAJE_ACTUALIZAR_USUARIO("Datos de usuario actualizados"),
	MENSAJE_ELIMINAR_USUARIO("Datos de usuario eliminados");
	
	private final String message;

	private MessageState(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
}
