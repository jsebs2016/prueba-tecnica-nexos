package com.nexossoftware.dto;

import java.io.Serializable;

public class ChargeDTO implements Serializable{

	private static final long serialVersionUID = 7134786945949873117L;
	private String idCharge;
	private String chargeName;
	
	public ChargeDTO() {
		super();
	}

	public ChargeDTO(String idCharge, String chargeName) {
		super();
		this.idCharge = idCharge;
		this.chargeName = chargeName;
	}

	public String getIdCharge() {
		return idCharge;
	}

	public void setIdCharge(String idCharge) {
		this.idCharge = idCharge;
	}

	public String getChargeName() {
		return chargeName;
	}

	public void setChargeName(String chargeName) {
		this.chargeName = chargeName;
	}
	
}
