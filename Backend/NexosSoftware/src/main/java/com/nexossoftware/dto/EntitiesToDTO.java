package com.nexossoftware.dto;

import com.nexossoftware.constant.CodeState;
import com.nexossoftware.constant.MessageState;
import com.nexossoftware.model.Charge;
import com.nexossoftware.model.Product;
import com.nexossoftware.model.User;

public class EntitiesToDTO {

	private EntitiesToDTO() {
	}
	
	public static MessageDTO sendResponseDTOCharge(Charge charge) {
		if(null != charge ) {
			return new MessageDTO(CodeState.CODIGO_EXITOSO.getCodigo(),MessageState.MENSAJE_EXITO_CARGO.getMessage());
		}
		return new MessageDTO(CodeState.CODIGO_ERROR.getCodigo(), MessageState.MENSAJE_FALLO_SERVIDOR.getMessage());
	}
	
	public static MessageDTO sendResponseDTOUser(User user) {
		if(null != user ) {
			return new MessageDTO(CodeState.CODIGO_EXITOSO.getCodigo(),MessageState.MENSAJE_EXITO_USUARIO.getMessage());
		}
		return new MessageDTO(CodeState.CODIGO_ERROR.getCodigo(), MessageState.MENSAJE_FALLO_SERVIDOR.getMessage());
	}
	
	public static MessageDTO sendResponseDTOProduct(Product product) {
		if(null != product ) {
			return new MessageDTO(CodeState.CODIGO_EXITOSO.getCodigo(),MessageState.MENSAJE_EXITO_PRODUCTO.getMessage());
		}
		return new MessageDTO(CodeState.CODIGO_ERROR.getCodigo(), MessageState.MENSAJE_FALLO_SERVIDOR.getMessage());
	}
	
	public static MessageDTO sendResponseDeleteDTOProduct(Integer val) {
		if(val == 1) {
			return new MessageDTO(CodeState.CODIGO_EXITOSO.getCodigo(),MessageState.MENSAJE_ELIMINAR_PRODUCTO.getMessage());
		}
		return new MessageDTO(CodeState.CODIGO_ERROR.getCodigo(), MessageState.MENSAJE_FALLO_SERVIDOR.getMessage());
	}
}

