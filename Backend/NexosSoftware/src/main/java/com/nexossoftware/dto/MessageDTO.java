package com.nexossoftware.dto;

import java.util.Objects;

public class MessageDTO {

	private String code;
	private String messageBody;
	
	public MessageDTO() {
		super();
	}
	
	public MessageDTO(String code, String messageBody) {
		super();
		this.code = code;
		this.messageBody = messageBody;
	}

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessageBody() {
		return messageBody;
	}
	public void setMessageBody(String messageBody) {
		this.messageBody = messageBody;
	}

	@Override
	public int hashCode() {
		return Objects.hash(code, messageBody);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MessageDTO other = (MessageDTO) obj;
		return Objects.equals(code, other.code) && Objects.equals(messageBody, other.messageBody);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MessageDTO [code=");
		builder.append(code);
		builder.append(", messageBody=");
		builder.append(messageBody);
		builder.append("]");
		return builder.toString();
	}
}
