package com.nexossoftware.dto;

import java.io.Serializable;

public class ProductDTO implements Serializable{

	private static final long serialVersionUID = 2330901978756972575L;
	private String idProduct;
	private String nameProduct;
	private String quantity;
	private String dateAdmision;
	private String idUser;
	private String nameUser;
	private String nameUserEdit;
	private String upgradeDate;
	
	public ProductDTO() {
		super();
	}

	public ProductDTO(String idProduct, String nameProduct, String quantity, String dateAdmision, String idUser,
			String nameUser, String nameUserEdit, String upgradeDate) {
		super();
		this.idProduct = idProduct;
		this.nameProduct = nameProduct;
		this.quantity = quantity;
		this.dateAdmision = dateAdmision;
		this.idUser = idUser;
		this.nameUser = nameUser;
		this.nameUserEdit = nameUserEdit;
		this.upgradeDate = upgradeDate;
	}

	public String getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(String idProduct) {
		this.idProduct = idProduct;
	}

	public String getNameProduct() {
		return nameProduct;
	}

	public void setNameProduct(String nameProduct) {
		this.nameProduct = nameProduct;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getDateAdmision() {
		return dateAdmision;
	}

	public void setDateAdmision(String dateAdmision) {
		this.dateAdmision = dateAdmision;
	}

	public String getIdUser() {
		return idUser;
	}

	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}

	public String getNameUser() {
		return nameUser;
	}

	public void setNameUser(String nameUser) {
		this.nameUser = nameUser;
	}

	public String getNameUserEdit() {
		return nameUserEdit;
	}

	public void setNameUserEdit(String nameUserEdit) {
		this.nameUserEdit = nameUserEdit;
	}

	public String getUpgradeDate() {
		return upgradeDate;
	}

	public void setUpgradeDate(String upgradeDate) {
		this.upgradeDate = upgradeDate;
	}
	
}
