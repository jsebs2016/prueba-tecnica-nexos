package com.nexossoftware.dto;

import java.io.Serializable;

public class UserDTO implements Serializable{

	private static final long serialVersionUID = 6907236532309834907L;
	private String idUser;
	private String nameUser;
	private String idCharge;
	private String age;
	private String dateAdmision;
	private String nameCharge;
	
	public UserDTO() {
		super();
	}
	
	public UserDTO(String idUser, String nameUser, String idCharge, String age, String dateAdmision,
			String nameCharge) {
		super();
		this.idUser = idUser;
		this.nameUser = nameUser;
		this.idCharge = idCharge;
		this.age = age;
		this.dateAdmision = dateAdmision;
		this.nameCharge = nameCharge;
	}

	public String getIdUser() {
		return idUser;
	}
	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}
	public String getNameUser() {
		return nameUser;
	}
	public void setNameUser(String nameUser) {
		this.nameUser = nameUser;
	}
	public String getIdCharge() {
		return idCharge;
	}
	public void setIdCharge(String idCharge) {
		this.idCharge = idCharge;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getDateAdmision() {
		return dateAdmision;
	}
	public void setDateAdmision(String dateAdmision) {
		this.dateAdmision = dateAdmision;
	}

	public String getNameCharge() {
		return nameCharge;
	}

	public void setNameCharge(String nameCharge) {
		this.nameCharge = nameCharge;
	}
	
	
	
	
}
