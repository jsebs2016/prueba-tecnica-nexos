package com.nexossoftware.exception;

public class NexosException extends Exception{

	private static final long serialVersionUID = -4377626767613499042L;

	public NexosException(String message, Throwable cause) {
		super(message, cause);
	}

	public NexosException(String message) {
		super(message);
	}

	public NexosException(Throwable cause) {
		super(cause);
	}

	
}
