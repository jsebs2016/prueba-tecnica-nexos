package com.nexossoftware.model;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cargo")
public class Charge {

	@Id
	@Column(name = "id_cargo")
	private Long idCharge;
	
	@Column(name = "nom_cargo")
	private String chargeName;

	public Charge() {
		super();
	}

	public Charge(Long idCharge, String chargeName) {
		super();
		this.idCharge = idCharge;
		this.chargeName = chargeName;
	}

	public Long getIdCharge() {
		return idCharge;
	}

	public void setIdCharge(Long idCharge) {
		this.idCharge = idCharge;
	}

	public String getChargeName() {
		return chargeName;
	}

	public void setChargeName(String chargeName) {
		this.chargeName = chargeName;
	}

	@Override
	public int hashCode() {
		return Objects.hash(chargeName, idCharge);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Charge other = (Charge) obj;
		return Objects.equals(chargeName, other.chargeName) && Objects.equals(idCharge, other.idCharge);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Charge [idCharge=").append(idCharge).append(", chargeName=").append(chargeName).append("]");
		return builder.toString();
	}
	
	
	
	
	
	
}
