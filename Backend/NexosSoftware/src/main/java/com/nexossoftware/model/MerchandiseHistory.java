package com.nexossoftware.model;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "historial_mercancia")
public class MerchandiseHistory {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_historial")
	private Long idHistory;

	@ManyToOne
	@JoinColumn(name = "id_mercancia")
	private Product product;
	
	@ManyToOne
	@JoinColumn(name = "id_usuario")
	private User user;
	
	@Column(name = "fecha_actualizacion")
	private Date upgradeDate;

	public MerchandiseHistory() {
		super();
	}

	public MerchandiseHistory(Long idHistory, Product product, User user, Date upgradeDate) {
		super();
		this.idHistory = idHistory;
		this.product = product;
		this.user = user;
		this.upgradeDate = upgradeDate;
	}

	public Long getIdHistory() {
		return idHistory;
	}

	public void setIdHistory(Long idHistory) {
		this.idHistory = idHistory;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Date getUpgradeDate() {
		return upgradeDate;
	}

	public void setUpgradeDate(Date upgradeDate) {
		this.upgradeDate = upgradeDate;
	}

	@Override
	public int hashCode() {
		return Objects.hash(idHistory, product, upgradeDate, user);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MerchandiseHistory other = (MerchandiseHistory) obj;
		return Objects.equals(idHistory, other.idHistory) && Objects.equals(product, other.product)
				&& Objects.equals(upgradeDate, other.upgradeDate) && Objects.equals(user, other.user);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MerchandiseHistory [idHistory=").append(idHistory).append(", product=").append(product)
				.append(", user=").append(user).append(", upgradeDate=").append(upgradeDate).append("]");
		return builder.toString();
	}
	
	
}
