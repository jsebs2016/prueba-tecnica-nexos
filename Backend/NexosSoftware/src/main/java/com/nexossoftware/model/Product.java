package com.nexossoftware.model;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "mercancia")
public class Product {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_mercancia")
	private Long idProduct;
	
	@Column(name = "nom_producto")
	private String productName;
	
	@Column(name = "cantidad")
	private Integer quantity;
	
	@Column(name = "fechaIngreso")
	private Date dateAdmision;
	
	@ManyToOne
	@JoinColumn(name = "id_usuario")
	private User user;

	public Product() {
		super();
	}

	public Product(Long idProduct, String productName, Integer quantity, Date dateAdmision, User user) {
		super();
		this.idProduct = idProduct;
		this.productName = productName;
		this.quantity = quantity;
		this.dateAdmision = dateAdmision;
		this.user = user;
	}

	public Long getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(Long idProduct) {
		this.idProduct = idProduct;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Date getDateAdmision() {
		return dateAdmision;
	}

	public void setDateAdmision(Date dateAdmision) {
		this.dateAdmision = dateAdmision;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public int hashCode() {
		return Objects.hash(dateAdmision, idProduct, productName, quantity, user);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		return Objects.equals(dateAdmision, other.dateAdmision) && Objects.equals(idProduct, other.idProduct)
				&& Objects.equals(productName, other.productName) && Objects.equals(quantity, other.quantity)
				&& Objects.equals(user, other.user);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Product [idProduct=").append(idProduct).append(", productName=").append(productName)
				.append(", quantity=").append(quantity).append(", dateAdmision=").append(dateAdmision).append(", user=")
				.append(user).append("]");
		return builder.toString();
	}
	
	
	
	
}
