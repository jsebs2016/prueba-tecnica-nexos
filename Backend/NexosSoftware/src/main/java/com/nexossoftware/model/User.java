package com.nexossoftware.model;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "usuario")
public class User {

	@Id
	@Column(name = "id_usuario")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idUsuario;
	
	@Column(name = "nom_usuario", columnDefinition = "TEXT")
	private String userName;
	
	@Column(name = "edad")
	private Short age;
	
	@Column(name = "fecha_ingreso")
	private Date dateAdmision;
	
	@ManyToOne
	@JoinColumn(name = "id_cargo")
	private Charge charge;

	public User() {
		super();
	}

	public User(Long idUsuario, String userName, Short age, Date dateAdmision, Charge charge) {
		super();
		this.idUsuario = idUsuario;
		this.userName = userName;
		this.age = age;
		this.dateAdmision = dateAdmision;
		this.charge = charge;
	}

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Short getAge() {
		return age;
	}

	public void setAge(Short age) {
		this.age = age;
	}

	public Date getDateAdmision() {
		return dateAdmision;
	}

	public void setDateAdmision(Date dateAdmision) {
		this.dateAdmision = dateAdmision;
	}

	public Charge getCharge() {
		return charge;
	}

	public void setCharge(Charge charge) {
		this.charge = charge;
	}

	@Override
	public int hashCode() {
		return Objects.hash(age, charge, dateAdmision, idUsuario, userName);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		return Objects.equals(age, other.age) && Objects.equals(charge, other.charge)
				&& Objects.equals(dateAdmision, other.dateAdmision) && Objects.equals(idUsuario, other.idUsuario)
				&& Objects.equals(userName, other.userName);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("User [idUsuario=").append(idUsuario).append(", userName=").append(userName).append(", age=")
				.append(age).append(", dateAdmision=").append(dateAdmision).append(", charge=").append(charge)
				.append("]");
		return builder.toString();
	}
	
	
	
	
}
