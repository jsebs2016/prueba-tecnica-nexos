package com.nexossoftware.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nexossoftware.model.Charge;

public interface ChargeDAO extends JpaRepository<Charge, Long> {

}
