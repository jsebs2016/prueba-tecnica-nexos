package com.nexossoftware.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.nexossoftware.model.MerchandiseHistory;

public interface MerchandiseHistoryDAO extends JpaRepository<MerchandiseHistory, Long>{
	
	@Query(value = "SELECT * FROM historial_mercancia where id_mercancia = ?1 ", nativeQuery = true)
	MerchandiseHistory getHistoryProduct(Long idMercancia);
}