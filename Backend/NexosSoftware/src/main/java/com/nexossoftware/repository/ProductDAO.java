package com.nexossoftware.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nexossoftware.model.Product;

public interface ProductDAO extends JpaRepository<Product, Long> {
	
	 List<Product> findByProductName(String productName);
}
