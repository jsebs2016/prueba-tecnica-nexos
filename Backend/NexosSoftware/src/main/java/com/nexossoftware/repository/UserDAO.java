package com.nexossoftware.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nexossoftware.model.User;

public interface UserDAO extends JpaRepository<User, Long>{

}
