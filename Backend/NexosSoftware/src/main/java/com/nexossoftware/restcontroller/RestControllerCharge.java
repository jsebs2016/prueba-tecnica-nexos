package com.nexossoftware.restcontroller;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nexossoftware.constant.MessageState;
import com.nexossoftware.dto.ChargeDTO;
import com.nexossoftware.dto.EntitiesToDTO;
import com.nexossoftware.dto.MessageDTO;
import com.nexossoftware.exception.NexosException;
import com.nexossoftware.model.Charge;
import com.nexossoftware.serviceimpl.ChargeServiceImpl;

@RestController
@RequestMapping("/api/chargecontroller")
@CrossOrigin(origins = "http://localhost:4200/", maxAge = 3600)
public class RestControllerCharge {
	
	private static final Logger log = LogManager.getLogger(RestControllerCharge.class); 

	@Autowired
	private ChargeServiceImpl chargeServiceImpl;
	
	@GetMapping(path = "/getcharges")
	public List<Charge> getListCharges(){
		List<Charge>listCharges = new ArrayList<>();
		log.info("<-------INICIO DE SERVICIO CONSULTAR CARGOS------>");
		try {
			listCharges= chargeServiceImpl.getAllCharges();
			log.info("<-------CONSULTA REALIZADA CORRECTAMENTE------>");
		} catch (NexosException e) {
			listCharges = null;
			log.error(MessageState.MENSAJE_ERROR_CONSULTA.getMessage(),e);
		}
		log.info("<-------FIN DE SERVICIO CONSULTAR CARGOS------>");
		return listCharges;
	}
	
	@PostMapping(path = "/insertcharges")
	public MessageDTO saveCharges(@RequestBody ChargeDTO chargeDTO) {
		log.info("<-------INICIO DE SERVICIO GUARDAR CARGOS------>");
		Charge charge = new Charge();
		try {
			charge = chargeServiceImpl.saveCharge(chargeDTO);
			log.info("<-------INSERCION REALIZADA CORRECTAMENTE------>");
		} catch (NexosException e) {
			charge = null;
			log.error("No fue posible almacenar cargos",e);
		}
		log.info("<-------FIN DE SERVICIO GUARDAR CARGOS------>");
		return EntitiesToDTO.sendResponseDTOCharge(charge);
	}
}
