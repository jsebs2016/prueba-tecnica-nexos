package com.nexossoftware.restcontroller;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nexossoftware.constant.MessageState;
import com.nexossoftware.dto.EntitiesToDTO;
import com.nexossoftware.dto.MessageDTO;
import com.nexossoftware.dto.ProductDTO;
import com.nexossoftware.exception.NexosException;
import com.nexossoftware.model.Product;
import com.nexossoftware.serviceimpl.ProductServiceImpl;

@RestController
@RequestMapping("/api/productcontroller")
@CrossOrigin(origins = "http://localhost:4200/", maxAge = 3600)
public class RestControllerProduct {

	private static final Logger log = LogManager.getLogger(RestControllerProduct.class); 
	
	@Autowired
	private ProductServiceImpl productServiceImpl;
	
	@GetMapping(path = "/getproducts")
	public List<ProductDTO> listProducts(){
		List<ProductDTO>listProducts = new ArrayList<>();
		log.info("<-------INICIO DE SERVICIO CONSULTAR PRODUCTOS------>");
		try {
			listProducts = productServiceImpl.getProducts();
			log.info("<-------CONSULTA REALIZADA CORRECTAMENTE------>");
		} catch (NexosException e) {
			listProducts = null;
			log.error(MessageState.MENSAJE_ERROR_CONSULTA.getMessage(),e);
		}
		log.info("<-------FIN DE SERVICIO CONSULTAR PRODUCTOS------>");
		return listProducts;
	}
	
	@GetMapping(path = "/getproductbyid/{idProduct}")
	public ProductDTO getProductById(@PathVariable(value = "idProduct") String idProduct) {
		ProductDTO productDTO = new ProductDTO();
		log.info("<-------INICIO DE SERVICIO CONSULTAR PRODUCTO POR ID------>");
		try {
			productDTO = productServiceImpl.getProductByID(idProduct);
			log.info("<-------CONSULTA REALIZADA CORRECTAMENTE------>");
		} catch (Exception e) {
			productDTO = null;
			log.error(MessageState.MENSAJE_ERROR_CONSULTA.getMessage(),e);
		}
		return productDTO;
	}

	@PostMapping(path = "/insertproduct")
	public MessageDTO saveProduct(@RequestBody ProductDTO productDTO) {
		Product product = new Product();
		log.info("<-------INICIO DE SERVICIO GUARDAR PRODUCTOS------>");
		try {
			product = productServiceImpl.saveProduct(productDTO);
			log.info("<-------INSERCION REALIZADA CORRECTAMENTE------>");
		} catch (NexosException e) {
			product = null;
			log.error("No fue posible almacenar producto",e);
		}
		log.info("<-------FIN DE SERVICIO GUARDAR PRODUCTOS------>");
		return EntitiesToDTO.sendResponseDTOProduct(product);
	}
	
	@PutMapping(path = "/updateproduct")
	public MessageDTO updateProduct(@RequestBody ProductDTO productDTO) {
		Product product = new Product();
		log.info("<-------INICIO DE SERVICIO ACTUALIZAR PRODUCTOS------>");
		try {
			product = productServiceImpl.updateProduct(productDTO);
			log.info("<-------ACTUALIZACION REALIZADA CORRECTAMENTE------>");
		} catch (NexosException e) {
			product = null;
			log.error("No fue posible actualizar producto",e);
		}
		log.info("<-------FIN DE SERVICIO ACTUALIZAR PRODUCTOS------>");
		return EntitiesToDTO.sendResponseDTOProduct(product);
	}
	
	@DeleteMapping(path = "/deleteproduct")
	public MessageDTO deleteProduct(@RequestBody ProductDTO productDTO) {
		Integer val = 0;
		log.info("<-------INICIO DE SERVICIO ELIMINAR PRODUCTOS------>");
		try {
			val = productServiceImpl.deleteProduct(productDTO);
			log.info("<-------ELIMINACION REALIZADA CORRECTAMENTE------>");
		} catch (Exception e) {
			val = -1;
			log.error("No fue posible eliminar el producto",e);
		}
		log.info("<-------FIN DE SERVICIO ELIMINAR PRODUCTOS------>");
		return EntitiesToDTO.sendResponseDeleteDTOProduct(val);
	}
	
}
