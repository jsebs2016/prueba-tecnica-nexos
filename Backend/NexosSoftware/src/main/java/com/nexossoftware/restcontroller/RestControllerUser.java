package com.nexossoftware.restcontroller;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nexossoftware.constant.MessageState;
import com.nexossoftware.dto.EntitiesToDTO;
import com.nexossoftware.dto.MessageDTO;
import com.nexossoftware.dto.UserDTO;
import com.nexossoftware.exception.NexosException;
import com.nexossoftware.model.User;
import com.nexossoftware.serviceimpl.UserServiceImpl;

@RestController
@RequestMapping("/api/usercontroller")
@CrossOrigin(origins = "http://localhost:4200/", maxAge = 3600)
public class RestControllerUser {

	private static final Logger log = LogManager.getLogger(RestControllerUser.class); 
	
	@Autowired
	private UserServiceImpl serviceImpl;
	
	@GetMapping(path = "/getusers")
	public List<UserDTO> getUsers(){
		List<UserDTO>listUsers = new ArrayList<>();
		log.info("<-------INICIO DE SERVICIO CONSULTAR USUARIOS------>");
		try {
			listUsers = serviceImpl.getUsers();
			log.info("<-------CONSULTA REALIZADA CORRECTAMENTE------>");
		} catch (NexosException e) {
			listUsers = null;
			log.error(MessageState.MENSAJE_ERROR_CONSULTA.getMessage(),e);
		}
		log.info("<-------FIN DE SERVICIO CONSULTAR USUARIOS------>");
		return listUsers;
	}
	
	@PostMapping(path = "/insertusers")
	public MessageDTO saveUser(@RequestBody UserDTO userDTO) {
		User user = new User();
		log.info("<-------INICIO DE SERVICIO GUARDAR USUARIOS------>");
		try {
			user = serviceImpl.saveUser(userDTO);
			log.info("<-------INSERCION REALIZADA CORRECTAMENTE------>");
		} catch (NexosException e) {
			user = null;
			log.error("No fue posible almacenar usuario",e);
		}
		log.info("<-------FIN DE SERVICIO GUARDAR USUARIOS------>");
		return EntitiesToDTO.sendResponseDTOUser(user);
	}
	
	
}
