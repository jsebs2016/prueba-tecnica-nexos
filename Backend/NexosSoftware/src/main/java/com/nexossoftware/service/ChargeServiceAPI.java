package com.nexossoftware.service;

import com.nexossoftware.commons.GenericServiceAPI;
import com.nexossoftware.model.Charge;

public interface ChargeServiceAPI extends GenericServiceAPI<Charge, Long>{

}
