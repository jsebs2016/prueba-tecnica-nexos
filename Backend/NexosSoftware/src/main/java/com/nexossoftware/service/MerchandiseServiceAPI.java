package com.nexossoftware.service;

import com.nexossoftware.commons.GenericServiceAPI;
import com.nexossoftware.model.MerchandiseHistory;

public interface MerchandiseServiceAPI extends GenericServiceAPI<MerchandiseHistory, Long>{

}
