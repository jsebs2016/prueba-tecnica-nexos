package com.nexossoftware.service;

import com.nexossoftware.commons.GenericServiceAPI;
import com.nexossoftware.model.Product;

public interface ProductServiceAPI extends GenericServiceAPI<Product, Long>{

}
