package com.nexossoftware.service;

import com.nexossoftware.commons.GenericServiceAPI;
import com.nexossoftware.model.User;

public interface UserServiceAPI extends GenericServiceAPI<User, Long> {

}
