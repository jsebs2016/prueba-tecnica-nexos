package com.nexossoftware.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.nexossoftware.commons.GenericServiceImpl;
import com.nexossoftware.constant.MessageState;
import com.nexossoftware.dto.ChargeDTO;
import com.nexossoftware.exception.NexosException;
import com.nexossoftware.model.Charge;
import com.nexossoftware.repository.ChargeDAO;
import com.nexossoftware.service.ChargeServiceAPI;

@Service
public class ChargeServiceImpl extends GenericServiceImpl<Charge, Long> implements ChargeServiceAPI{

	private static final Logger log = LogManager.getLogger(ChargeServiceImpl.class); 
	@Autowired
	private ChargeDAO chargeDao;
	
	@Override
	public JpaRepository<Charge, Long> getDao() {
		return chargeDao;
	}
	
	public List<Charge> getAllCharges() throws NexosException{
		List<Charge>listCharges = new ArrayList<>();
		try {
			listCharges = getAll();
		} catch (Exception e) {
			log.error(MessageState.MENSAJE_ERROR_CONSULTA.getMessage(), e);
			throw new NexosException(e);
		}
		return listCharges;
	}
	
	public Charge saveCharge(ChargeDTO chargeDTO) throws NexosException {
		Charge charge = new Charge();
		try {
			charge.setChargeName(chargeDTO.getChargeName());
			charge.setIdCharge(Long.parseLong(chargeDTO.getIdCharge()));
			charge = save(charge);
		} catch (Exception e) {
			log.error(MessageState.MENSAJE_ERROR_CARGO.getMessage(), e);
			throw new NexosException(e);
		}
		return charge;
	}

}
