package com.nexossoftware.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.nexossoftware.commons.GenericServiceImpl;
import com.nexossoftware.model.MerchandiseHistory;
import com.nexossoftware.repository.MerchandiseHistoryDAO;
import com.nexossoftware.service.MerchandiseServiceAPI;

@Service
public class MerchandiseServiceImpl extends GenericServiceImpl<MerchandiseHistory, Long>
		implements MerchandiseServiceAPI {

	@Autowired
	private MerchandiseHistoryDAO merchandiseDao;

	@Override
	public JpaRepository<MerchandiseHistory, Long> getDao() {
		return  merchandiseDao;
	}
}
