package com.nexossoftware.serviceimpl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.nexossoftware.commons.GenericServiceImpl;
import com.nexossoftware.constant.MessageState;
import com.nexossoftware.dto.ProductDTO;
import com.nexossoftware.exception.NexosException;
import com.nexossoftware.model.MerchandiseHistory;
import com.nexossoftware.model.Product;
import com.nexossoftware.model.User;
import com.nexossoftware.repository.MerchandiseHistoryDAO;
import com.nexossoftware.repository.ProductDAO;
import com.nexossoftware.service.ProductServiceAPI;

@Service
public class ProductServiceImpl extends GenericServiceImpl<Product, Long> implements ProductServiceAPI {

	private static final Logger log = LogManager.getLogger(ProductServiceImpl.class);
	@Autowired
	private ProductDAO productDao;

	@Autowired
	private MerchandiseHistoryDAO merchandiseDao;

	@Override
	public JpaRepository<Product, Long> getDao() {
		return productDao;
	}

	public List<ProductDTO> getProducts() throws NexosException {
		List<Product> listProducts = new ArrayList<>();
		List<ProductDTO> listResponse = new ArrayList<>();
		try {
			listProducts = getAll();
			listProducts.forEach(response -> {
				ProductDTO productDTO = new ProductDTO();
				MerchandiseHistory history = new MerchandiseHistory();
				productDTO.setDateAdmision(convertDateToString(response.getDateAdmision()));
				productDTO.setIdUser(response.getUser().getIdUsuario().toString());
				productDTO.setNameUser(response.getUser().getUserName());
				productDTO.setQuantity(response.getQuantity().toString());
				productDTO.setIdProduct(response.getIdProduct().toString());
				productDTO.setNameProduct(response.getProductName());
				history = getMerchandise(response.getIdProduct());
				if (history != null) {
					productDTO.setNameUserEdit(history.getUser().getUserName());
					productDTO.setUpgradeDate(convertDateToString(history.getUpgradeDate()));
				} else {
					productDTO.setNameUserEdit("");
					productDTO.setUpgradeDate("");
				}
				listResponse.add(productDTO);
			});
		} catch (Exception e) {
			log.error(MessageState.MENSAJE_ERROR_CONSULTA.getMessage());
			throw new NexosException(MessageState.MENSAJE_ERROR_CONSULTA.getMessage(), e);
		}
		return listResponse;
	}
	
	public ProductDTO getProductByID(String idProduct) throws NexosException{
		ProductDTO productDTO = new ProductDTO();
		Product product = new Product();
		try {
			product = get(Long.parseLong(idProduct));
			productDTO.setDateAdmision(convertDateToString(product.getDateAdmision()));
			productDTO.setIdUser(product.getUser().getIdUsuario().toString());
			productDTO.setNameUser(product.getUser().getUserName());
			productDTO.setQuantity(product.getQuantity().toString());
			productDTO.setIdProduct(product.getIdProduct().toString());
			productDTO.setNameProduct(product.getProductName());
		} catch (Exception e) {
			log.error(MessageState.MENSAJE_ERROR_CONSULTA.getMessage());
			throw new NexosException(MessageState.MENSAJE_ERROR_CONSULTA.getMessage(), e);
		}
		return productDTO;
		
	}

	public Product updateProduct(ProductDTO productDTO) throws NexosException {
		Product product = new Product();
		User user = new User();
		MerchandiseHistory history = new MerchandiseHistory();
		try {
			if (productDTO.getNameProduct() != null || productDTO.getNameProduct().equals("")) {
				product.setIdProduct(Long.parseLong(productDTO.getIdProduct()));
				user.setIdUsuario(Long.parseLong(productDTO.getIdUser()));
				product.setUser(user);
				product.setQuantity(Integer.parseInt(productDTO.getQuantity()));
				product.setProductName(productDTO.getNameProduct());
				product.setDateAdmision(getFormatDate(productDTO.getDateAdmision()));

				product = getDao().save(product);

				user.setIdUsuario(Long.parseLong(productDTO.getNameUserEdit()));
				history.setProduct(product);
				history.setUser(user);
				history.setUpgradeDate(getDateFromSystem());
				merchandiseDao.save(history);

			}
		} catch (Exception e) {
			log.error(MessageState.MENSAJE_ERROR_PRODUCTO.getMessage());
			throw new NexosException(MessageState.MENSAJE_ERROR_PRODUCTO.getMessage(), e);
		}
		return product;
	}
	
	public Integer deleteProduct(ProductDTO productDTO) throws NexosException {
		Product product = new Product();
		MerchandiseHistory history = new MerchandiseHistory();
		try {
			product = get(Long.parseLong(productDTO.getIdProduct()));
			if (product.getUser().getIdUsuario().longValue() == Long.parseLong( productDTO.getIdUser())) {
				history = getMerchandise(Long.parseLong(productDTO.getIdProduct()));
				if(null != history) {
					merchandiseDao.delete(history);
				}
				delete(product.getIdProduct());
				return 1;
			}
		} catch (Exception e) {
			log.error(MessageState.MENSAJE_ERROR_PRODUCTO.getMessage());
			throw new NexosException(MessageState.MENSAJE_ERROR_PRODUCTO.getMessage(), e);
		}
		return -1;
	}

	public Product saveProduct(ProductDTO productDTO) throws NexosException {
		Product product = new Product();
		User user = new User();
		try {
			if (productDTO.getNameProduct() != null || productDTO.getNameProduct().equals("")) {
				List<Product> listProduct = productDao.findByProductName(productDTO.getNameProduct());
				if (listProduct.isEmpty()) {
					user.setIdUsuario(Long.parseLong(productDTO.getIdUser()));
					product.setUser(user);
					product.setQuantity(Integer.parseInt(productDTO.getQuantity()));
					product.setProductName(productDTO.getNameProduct());
					product.setDateAdmision(getFormatDate(productDTO.getDateAdmision()));
					product = save(product);
				}
			}
		} catch (Exception e) {
			log.error(MessageState.MENSAJE_ERROR_PRODUCTO.getMessage());
			throw new NexosException(MessageState.MENSAJE_ERROR_PRODUCTO.getMessage(), e);
		}
		return product;
	}

	private MerchandiseHistory getMerchandise(Long idProduct) {
		MerchandiseHistory merchandiseHistory = merchandiseDao.getHistoryProduct(idProduct);
		if (merchandiseHistory != null) {
			return merchandiseHistory;
		}
		return null;
	}

	private Date getDateFromSystem() throws NexosException {
		LocalDate date = LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
		String formattedString = date.format(formatter);
		try {
			return new SimpleDateFormat("yyyy/MM/dd").parse(formattedString);
		} catch (ParseException e) {
			throw new NexosException(e);
		}
	}

	private Date getFormatDate(String dateString) throws NexosException {
		Date dateFormat = new Date();
		try {
			DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
			DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd", Locale.ENGLISH);
			LocalDate date = LocalDate.parse(dateString, inputFormatter);
			String formattedDate = outputFormatter.format(date);
			dateFormat = new SimpleDateFormat("yyyy/MM/dd").parse(formattedDate);
		} catch (ParseException e) {
			throw new NexosException(e);
		}  
		return dateFormat;
	}

	private String convertDateToString(Date date) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		return dateFormat.format(date);
	}

}
