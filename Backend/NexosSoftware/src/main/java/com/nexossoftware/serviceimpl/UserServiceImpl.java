package com.nexossoftware.serviceimpl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.nexossoftware.commons.GenericServiceImpl;
import com.nexossoftware.constant.MessageState;
import com.nexossoftware.dto.UserDTO;
import com.nexossoftware.exception.NexosException;
import com.nexossoftware.model.Charge;
import com.nexossoftware.model.User;
import com.nexossoftware.repository.UserDAO;
import com.nexossoftware.service.UserServiceAPI;

@Service
public class UserServiceImpl extends GenericServiceImpl<User, Long> implements UserServiceAPI{

	private static final Logger log = LogManager.getLogger(UserServiceImpl.class); 
	@Autowired
	private UserDAO userDao;
	
	@Override
	public JpaRepository<User, Long> getDao() {
		return userDao;
	}
	
	public List<UserDTO> getUsers() throws NexosException{
		List<User>listUsers = new ArrayList<>();
		List<UserDTO> listResponse = new ArrayList<>();
		try {
			listUsers = getAll();
			listUsers.forEach(response -> {
				UserDTO userDTO = new UserDTO();
				userDTO.setDateAdmision(convertDateToString(response.getDateAdmision()));
				userDTO.setAge(response.getAge().toString());
				userDTO.setIdUser(response.getIdUsuario().toString());
				userDTO.setNameUser(response.getUserName());
				userDTO.setIdCharge(response.getCharge().getIdCharge().toString());
				userDTO.setNameCharge(response.getCharge().getChargeName());
				listResponse.add(userDTO);
			});
		} catch (Exception e) {
			log.error(MessageState.MENSAJE_ERROR_CONSULTA.getMessage(), e);
			throw new NexosException(e);
		}
		return listResponse;	
	}
	
 	public User saveUser(UserDTO userDTO) throws NexosException{
		User user = new User();
		Charge charge = new Charge(); 
		try { 
			charge.setIdCharge(Long.parseLong(userDTO.getIdCharge()));
			user.setAge(Short.parseShort(userDTO.getAge()));
			user.setCharge(charge);
			user.setDateAdmision(getFormatDate(userDTO.getDateAdmision()));
			user.setUserName(userDTO.getNameUser());
			user = save(user);
		} catch (Exception e) {
			log.error(MessageState.MENSAJE_ERROR_USUARIO.getMessage());
			throw new NexosException(MessageState.MENSAJE_ERROR_USUARIO.getMessage(), e);
		}
		return user;
	}
	
	private Date getFormatDate(String dateString) throws NexosException {
		Date dateFormat = new Date();
		try {
			DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
			DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd", Locale.ENGLISH);
			LocalDate date = LocalDate.parse(dateString, inputFormatter);
			String formattedDate = outputFormatter.format(date);
			dateFormat = new SimpleDateFormat("yyyy/MM/dd").parse(formattedDate);
		} catch (ParseException e) {
			throw new NexosException(e);
		}  
		return dateFormat;
	}
	
	private String convertDateToString(Date date) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		return dateFormat.format(date);
	}

}
