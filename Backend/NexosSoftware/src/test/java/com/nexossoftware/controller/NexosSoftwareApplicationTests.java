package com.nexossoftware.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nexossoftware.dto.ChargeDTO;
import com.nexossoftware.dto.UserDTO;
import com.nexossoftware.main.NexosSoftwareApplication;
import com.nexossoftware.model.Charge;
import com.nexossoftware.model.User;
import com.nexossoftware.serviceimpl.ChargeServiceImpl;
import com.nexossoftware.serviceimpl.UserServiceImpl;

@SpringBootTest(classes = NexosSoftwareApplication.class)
@AutoConfigureMockMvc
class NexosSoftwareApplicationTests {

	@MockBean
	public ChargeServiceImpl chargeServiceImpl;

	@MockBean
	public UserServiceImpl userServiceImpl;

	@Autowired
	MockMvc mockMvc;

	@Autowired
	ObjectMapper mapper;

	@DisplayName("Test Spring @Autowired ShowCharges")
	@Test
	void showAllCharges() throws Exception {
		List<Charge> listCharges = new ArrayList<>();
		Charge charge1 = new Charge((long) 101, "Asesor de ventas");
		Charge charge2 = new Charge((long) 102, "Administrador");
		Charge charge3 = new Charge((long) 103, "Soporte");
		listCharges.add(charge1);
		listCharges.add(charge2);
		listCharges.add(charge3);
		Mockito.when(chargeServiceImpl.getAllCharges()).thenReturn(listCharges);

		mockMvc.perform(
				MockMvcRequestBuilders.get("/api/chargecontroller/getcharges").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$[0].idCharge", is(101)))
				.andExpect(jsonPath("$[0].chargeName", is("Asesor de ventas")))
				.andExpect(jsonPath("$[1].idCharge", is(102)))
				.andExpect(jsonPath("$[1].chargeName", is("Administrador")))
				.andExpect(jsonPath("$[2].idCharge", is(103))).andExpect(jsonPath("$[2].chargeName", is("Soporte")));
	}

	@DisplayName("Test Spring @Autowired createCharges")
	@Test
	void addCharges() throws JacksonException, Exception {
		Charge charge = new Charge((long) 104, "Ingeniero");
		ChargeDTO chargeDTO = new ChargeDTO("104", "Ingeniero");

		Mockito.when(chargeServiceImpl.save(Mockito.any(Charge.class))).thenReturn(charge);

		MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/api/chargecontroller/insertcharges")
				.contentType(MediaType.APPLICATION_JSON).content(this.mapper.writeValueAsBytes(chargeDTO));

		mockMvc.perform(builder).andExpect(status().isOk());
	}

	@DisplayName("Test Spring @Autowired ShowUsers")
	@Test
	void showAllUsers() throws Exception {
		List<UserDTO> listUsers = new ArrayList<>();
		UserDTO user = new UserDTO("15", "Juan Sebastian", "101", "25", "2021/01/03", "Asesor de ventas");
		listUsers.add(user);
		Mockito.when(userServiceImpl.getUsers()).thenReturn(listUsers);

		mockMvc.perform(
				MockMvcRequestBuilders.get("/api/usercontroller/getusers").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$[0].idCharge", is("101")))
				.andExpect(jsonPath("$[0].nameCharge", is("Asesor de ventas")));
	}

	@DisplayName("Test Spring @Autowired createUsers")
	@Test
	void addUsers() throws JacksonException, Exception {
		UserDTO userDTO = new UserDTO("", "Juanita romero", "102", "24", "2021/02/08", "");
		Charge charge = new Charge((long) 102, "Administrador");
		
		Date dateFormat = new Date();
		DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd", Locale.ENGLISH);
		LocalDate date = LocalDate.parse("2021/02/08",outputFormatter);
		String formattedDate = outputFormatter.format(date);
		dateFormat = new SimpleDateFormat("yyyy/MM/dd").parse(formattedDate);
		User user = new User(null, "Juanita romero", (short) 24, dateFormat, charge);

		Mockito.when(userServiceImpl.saveUser(Mockito.any(UserDTO.class))).thenReturn(user);

		MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/api/chargecontroller/insertcharges")
				.contentType(MediaType.APPLICATION_JSON).content(this.mapper.writeValueAsBytes(userDTO));

		mockMvc.perform(builder).andExpect(status().isOk());
	}

}
