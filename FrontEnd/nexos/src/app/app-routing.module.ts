import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChargeListComponent } from './charge/charge-list/charge-list.component';
import { CreateChargeComponent } from './charge/create-charge/create-charge.component';
import { CreateProductComponent } from './product/create-product/create-product.component';
import { ProductListComponent } from './product/product-list/product-list.component';
import { UpdateProductComponent } from './product/update-product/update-product.component';
import { UserCreateComponent } from './user/user-create/user-create.component';
import { UserListComponent } from './user/user-list/user-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'productos', pathMatch: 'full' },
  { path:'agregarCargos', component:CreateChargeComponent},
  { path:'cargos', component:ChargeListComponent},
  { path:'usuarios', component:UserListComponent},
  { path:'agregarUsuarios', component:UserCreateComponent},
  { path:'productos', component:ProductListComponent},
  { path:'agregarProductos', component:CreateProductComponent},
  { path:'actualizarProductos/:idProduct', component:UpdateProductComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
