import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Charge } from 'src/app/model/charge.model';
import { ChargeServiceService } from 'src/app/service/charge-service.service';

@Component({
  selector: 'app-charge-list',
  templateUrl: './charge-list.component.html',
  styleUrls: ['./charge-list.component.scss']
})
export class ChargeListComponent implements OnInit {

  displayedColumns: string[] = ['idCharge', 'chargeName'];
  ChargeData: any=[];
  dataSource = new MatTableDataSource<Charge>();
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort

  constructor(private chargeService: ChargeServiceService, private router: Router,  private changeDetectorRefs: ChangeDetectorRef) { 
  }
  ngOnInit(): void {
    this.chargeService.getCharges().subscribe( data=> {
      this.ChargeData = data;
      this.dataSource = new MatTableDataSource<Charge>(this.ChargeData);
      this.changeDetectorRefs.detectChanges();
   });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
