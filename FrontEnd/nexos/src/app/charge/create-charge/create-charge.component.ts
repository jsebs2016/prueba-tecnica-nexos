import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Charge } from 'src/app/model/charge.model';
import { ChargeServiceService } from 'src/app/service/charge-service.service';

@Component({
  selector: 'app-create-charge',
  templateUrl: './create-charge.component.html',
  styleUrls: ['./create-charge.component.scss']
})
export class CreateChargeComponent implements OnInit {

  charge:Charge = new Charge();
  submitted = false;

  constructor(private chargeService: ChargeServiceService,private router: Router) { }

  ngOnInit(): void {
  }

  onSubmit(){
    this.submitted = true;
    this.chargeService.createCharge(this.charge)
      .subscribe(data => console.log(data), error => console.log(error));
    this.charge = new Charge();
    this.router.navigate(['/cargos']);
  }

}
