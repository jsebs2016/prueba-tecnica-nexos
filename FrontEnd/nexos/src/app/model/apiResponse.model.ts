export class ApiResponse{
    status:string;
    message:string;
    result:any;
    constructor(){
        this.status = "";
        this.message = "";
    }
}