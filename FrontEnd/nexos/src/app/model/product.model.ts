export class Product{
    idProduct : string;
	nameProduct : string;
	quantity : string;
	idUser : string;
	dateAdmision : string;
	nameUser : string;
    nameUserEdit : string;
    upgradeDate : string;

    constructor(){
        this.idProduct = "";
        this.nameUser = "";
        this.nameProduct = "";
        this.quantity = "";
        this.dateAdmision = "";
        this.idUser = "";
        this.nameUserEdit = "";
        this.upgradeDate = "";
    }
}