import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Product } from 'src/app/model/product.model';
import { ProductServiceService } from 'src/app/service/product-service.service';
import { UserServiceService } from 'src/app/service/user-service.service';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.scss']
})
export class CreateProductComponent implements OnInit {

  product: Product = new Product();
  submitted = false;
  maxDate = new Date();
  users  = new Observable<any>();

  constructor(private productService: ProductServiceService, private router: Router, private userService: UserServiceService) { }

  ngOnInit(): void {  
    this.users = this.userService.getAllUserts();
  }

  onSubmit(){
    console.log(this.product);
    this.submitted = true;
    this.productService.createProduct(this.product)
      .subscribe(data => console.log(data), error => console.log(error));
    this.product = new Product();
    this.router.navigate(['/productos']);
  }

}
