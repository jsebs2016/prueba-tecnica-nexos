import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Product } from 'src/app/model/product.model';
import { ProductServiceService } from 'src/app/service/product-service.service';
import { UserServiceService } from 'src/app/service/user-service.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  displayedColumns: string[] = ['idProduct', 'nameProduct', 'quantity', 'dateAdmision', 'nameUser', 'nameUserEdit', 'upgradeDate', 'acciones'];
  productData: any = [];
  dataSource = new MatTableDataSource<Product>();
  users = new Observable<any>();
  product: Product = new Product();

  constructor(private productService: ProductServiceService, private userService: UserServiceService, private changeDetectorRefs: ChangeDetectorRef,
    private router: Router) {
    this.getAllProducts();
  }

  ngOnInit(): void {
    this.users = this.userService.getAllUserts();
  }

  deleteProduct(idUser: string, idProduct: string) {
    this.product.idUser = idUser;
    this.product.idProduct = idProduct;
    this.productService.deleteProduct(this.product)
      .subscribe(data => console.log(data), error => console.log(error));
    this.product = new Product();
    this.getAllProducts();
  }

  getAllProducts() {
    this.productService.getAllProducts().subscribe(data => {
      this.productData = data;
      this.dataSource = new MatTableDataSource<Product>(this.productData);
      this.changeDetectorRefs.detectChanges();
    });
  }

  updateProduct(idProduct: string) {
    this.router.navigate(['actualizarProductos', idProduct]);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


}
