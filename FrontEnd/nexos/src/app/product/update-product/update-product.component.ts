import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Product } from 'src/app/model/product.model';
import { ProductServiceService } from 'src/app/service/product-service.service';
import { UserServiceService } from 'src/app/service/user-service.service';

@Component({
  selector: 'app-update-product',
  templateUrl: './update-product.component.html',
  styleUrls: ['./update-product.component.scss']
})
export class UpdateProductComponent implements OnInit {

  idProduct: string = "";
  product: Product = new Product();
  submitted = false;
  maxDate = new Date();
  users = new Observable<any>();

  constructor(private productService: ProductServiceService, private route: Router,
    private activeRoute: ActivatedRoute, private userService: UserServiceService) { }

  ngOnInit(): void {
    this.users = this.userService.getAllUserts();
    this.product = new Product();
    this.idProduct = this.activeRoute.snapshot.params['idProduct'];
    this.productService.getProductByID(this.idProduct).subscribe(data => {
      console.log(data)
      this.product = data;
    }, error => console.log(error));
  }


  onSubmit() {
    console.log(this.product);
    this.productService.updateProduct(this.product)
      .subscribe(data => console.log(data), error => console.log(error));
    this.product = new Product();
    this.route.navigate(['/productos']);
  }


  list() {
    this.route.navigate(['productos']);
  }

}
