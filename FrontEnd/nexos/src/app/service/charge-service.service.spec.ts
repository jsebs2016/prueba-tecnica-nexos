import { TestBed } from '@angular/core/testing';

import { ChargeServiceService } from './charge-service.service';

describe('ChargeServiceService', () => {
  let service: ChargeServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ChargeServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
