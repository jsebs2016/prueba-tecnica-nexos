import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResponse } from '../model/apiResponse.model';
import { Charge } from '../model/charge.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ChargeServiceService {

  constructor(private http: HttpClient) { }
  private baseUrl: string = 'http://localhost:8080/api/chargecontroller/';

  createCharge(charge: Charge):Observable<ApiResponse>{
    return this.http.post<ApiResponse>(this.baseUrl+'insertcharges', charge);
  }
  

  getCharges():Observable<Charge>{
    return this.http.get<Charge>(this.baseUrl+'getcharges');
  }
}
