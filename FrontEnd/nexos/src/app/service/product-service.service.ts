import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResponse } from '../model/apiResponse.model';
import { Product } from '../model/product.model';

@Injectable({
  providedIn: 'root'
})
export class ProductServiceService {

  constructor(private http: HttpClient) { }
  private baseUrl: string = 'http://localhost:8080/api/productcontroller/';

  getProductByID(idProduct: string):Observable<Product>{
    return this.http.get<Product>(this.baseUrl+'getproductbyid/'+idProduct)
  }

  getAllProducts():Observable<Product>{
    return this.http.get<Product>(this.baseUrl+'getproducts');
  }

  createProduct(product: Product):Observable<ApiResponse>{
    return this.http.post<ApiResponse>(this.baseUrl+'insertproduct', product);
  }

  updateProduct(product: Product):Observable<ApiResponse>{
    return this.http.put<ApiResponse>(this.baseUrl+'updateproduct', product);
  }

  deleteProduct(product: Product):Observable<ApiResponse>{
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        idUser: product.idUser,
        idProduct: product.idProduct,
      },
    };
    console.log(options);
    return this.http.delete<ApiResponse>(this.baseUrl+'deleteproduct',options);
  }
}
