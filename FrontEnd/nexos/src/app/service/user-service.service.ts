import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResponse } from '../model/apiResponse.model';
import { User } from '../model/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  constructor(private http: HttpClient) { }
  private baseUrl: string = 'http://localhost:8080/api/usercontroller/';

  getAllUserts():Observable<User>{
    return this.http.get<User>(this.baseUrl+'getusers');
  }

  createUser(user: User):Observable<ApiResponse>{
    return this.http.post<ApiResponse>(this.baseUrl+'insertusers', user);
  }
}
