import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { User } from 'src/app/model/user.model';
import { ChargeServiceService } from 'src/app/service/charge-service.service';
import { UserServiceService } from 'src/app/service/user-service.service';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.scss']
})
export class UserCreateComponent implements OnInit {

  user: User = new User();
  submitted = false;
  maxDate = new Date();
  charges = new Observable<any>();


  constructor(private userService: UserServiceService, private router: Router, private chargeService: ChargeServiceService) {
  }

  ngOnInit(): void {
    this.charges = this.chargeService.getCharges();

  }

  onSubmit() {
    this.submitted = true;
    this.userService.createUser(this.user)
      .subscribe(data => console.log(data), error => console.log(error));
    this.user = new User();
    this.router.navigate(['/usuarios']);
  }

}
