import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { User } from 'src/app/model/user.model';
import { UserServiceService } from 'src/app/service/user-service.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  
  displayedColumns: string[] = ['nameUser', 'age','dateAdmision','nameCharge'];
  userData: any=[];
  dataSource = new MatTableDataSource<User>();

  constructor(private userService:UserServiceService, private router: Router) { this.userService.getAllUserts().subscribe(data=>{
    this.userData = data;
    this.dataSource = new MatTableDataSource<User>(this.userData);
  });}

  ngOnInit(): void {
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
